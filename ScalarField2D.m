
function ScalarField2D

% Baigtiniu elementu metodu sprendzia stacionaru silumos laidumo uzdavini

clc, close all

set(0,'DefaultFigureColormap',feval('hot'));

%cor = [7 7; 2 7; 2 1; 7 1; 4.5 4];
%ind = [1 2 5; 2 3 5; 3 4 5; 4 1 5];
%temp_given = 350;

%body_source = [150000 150000 0 150000];
%lmbd = [51 24 24 51];

%IS = [1 0 0 0 0];

%q = '?';
%alpha = '?';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % geometry for two elements
cor=[0 0; 2 0; 1 1; 0 3 ]  % konstrukcijos mazgu koordinates
ind=[1 2 3; 1 3 4]         % elementai
body_source=[0 3000]      % turiniai silumos saltiniai kiekviename elemente 
lmbd=[50 50 ]              % elementu silumos laidumo koeficientai 
q=[1 2 1000;               % poveikis: silumos srauto tankiai. Krastines nurodantys mazgu nr ir reiksmes 
   1 4 0    ]
alpha=[2 3 20 270;         % konvekciniai silumos mainai. Krastines nurodantys mazgu nr, konvekcijos koef ir aplinkos temperatura(K)  
       3 4 20 270]
IS=[0 0 0 1];              % sablonas duotu temperaturu taskams pazymeti 
temp_given = 500;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % geometry for one element
% cor=[0 0; 1 1; 0 3 ]  % konstrukcijos mazgu koordinates
% ind=[ 1 2 3]         % elementai
% 
% body_source=[3000]      % turiniai silumos saltiniai kiekviename elemente 
% lmbd=[50  ]              % elementu silumos laidumo koeficientai 
% q=[1 3 0 ]           % poveikis: silumos srauto tankiai. Krastines nurodantys mazgu nr ir reiksmes 
%    
% alpha=[2 1 20 270;2 3 20 270];         % konvekciniai silumos mainai. Krastines nurodantys mazgu nr, konvekcijos koef ir aplinkos temperatura(K)  
%    
% IS=[0 1 0];              % sablonas duotu temperaturu taskams pazymeti 
%temp_given = 300;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nel=size(ind,1)            % elementu skaicius 
nmz=size(cor,1)            % mazgu skaicius 
nq=size(q,1);nalpha=size(alpha,1);


T=zeros(nmz,1);T(find(IS))=temp_given; % suteikiama duota temperatura

figure(1);hold on;axis equal; title('Geometry of the Structure');
konstrukcijos_vaizdavimas(cor,ind,lmbd,body_source,q,alpha,T,IS,0);  

K=zeros(nmz,nmz);F=zeros(nmz,1);  % pasiruosiama surinkti konstrukcijos silumos laidumo matrica ir mazgu galiu vektoriu
for i=1:nel

    % elemento laidumo matrica del medziagos silumos laidumo:
 [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,:),1)),mean(cor(ind(i,:),2)) ]); % eilinio elemento geometriniai parametrai ir formos funkcijos
 Ke=zeros(3,3);Fe=zeros(3,1); % paruosiama elemento silumos laidumo matrica ir galiu vektorius
 D=[lmbd(i) 0;0 lmbd(i)];     % silumos laidumo konstantu matrica 
 B=[b;c];                     % matrica  B gradientui apskaiciuoti
 Ke=A*B'*D*B;                 % eilinio elemento matrica  
 
    % elemento mazgu galiu dedamosios del turiniu silumos saltiniu:
 Fe=Fe+ones(3,1)/3*A*body_source(i);
       
    % elemento mazgu galiu dedamosios del duotu srautu tankiu per krastines (srauto tankis teigiamas, kai iseina is kuno): 
  for j=1:nq                   
     if sum(sort(ind(i,[1,2])) == sort(q(j,1:2)))==2   % ar abu srautu veikiamos j-os krastines mazgai priklauso nagrinejamo elemento "12" krastinei 
      [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,[1,2]),1)),mean(cor(ind(i,[1,2]),2)) ]); % krastines "12" viduryje apskaiciuojama formos funkciju matricos reiksme
      Fe=Fe-s(1)*q(j,3)*N';  % per "12" krastine patenkanti silumos galia paverciama mazgu galiomis ir pridedama prie elemento silumos galiu vektoriaus
     end
     if sum(sort(ind(i,[2,3])) == sort(q(j,1:2)))==2   % ar abu srautu veikiamos j-os krastines mazgai priklauso nagrinejamo elemento "23" krastinei 
      [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,[2,3]),1)),mean(cor(ind(i,[2,3]),2)) ]);
      Fe=Fe-s(2)*q(j,3)*N';
     end
     if sum(sort(ind(i,[3,1])) == sort(q(j,1:2)))==2   % ar abu srautu veikiamos j-os krastines mazgai priklauso nagrinejamo elemento "31" krastinei 
      [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,[3,1]),1)),mean(cor(ind(i,[3,1]),2)) ]);
      Fe=Fe-s(3)*q(j,3)*N';
     end
  end

     % elemento laidumo matricos ir mazgu galiu dedamosios del konvekciniu mainu per krastines: 
    for j=1:nalpha                  
     if sum(sort(ind(i,[1,2])) == sort(alpha(j,1:2)))==2   % ar abu konvekcijos veikiamos j-os krastines mazgai priklauso nagrinejamo elemento "12" krastinei 
      [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,[1,2]),1)),mean(cor(ind(i,[1,2]),2)) ]); % krastines "12" viduryje apskaiciuojama formos funkciju matricos reiksme
      Fe=Fe+s(1)*alpha(j,3)*alpha(j,4)*N';  % per "12" krastine patenkanti silumos galia del konvekciniu mainu paverciama mazgu galiomis ir pridedama prie elemento silumos galiu vektoriaus
      Ke=Ke+s(1)*alpha(j,3)*N'*N; % laidumo matricos dedamoji del konvekciniu mainu
     end
     if sum(sort(ind(i,[2,3])) == sort(alpha(j,1:2)))==2   % ar abu konvekcijos veikiamos j-os krastines mazgai priklauso nagrinejamo elemento "12" krastinei 
      [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,[2,3]),1)),mean(cor(ind(i,[2,3]),2)) ]); % krastines "23" viduryje apskaiciuojama formos funkciju matricos reiksme
      Fe=Fe+s(2)*alpha(j,3)*alpha(j,4)*N';  
      Ke=Ke+s(2)*alpha(j,3)*N'*N; 
     end
     if sum(sort(ind(i,[3,1])) == sort(alpha(j,1:2)))==2   % ar abu konvekcijos veikiamos j-os krastines mazgai priklauso nagrinejamo elemento "12" krastinei 
      [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,[3,1]),1)),mean(cor(ind(i,[3,1]),2)) ]); % krastines "31" viduryje apskaiciuojama formos funkciju matricos reiksme
      Fe=Fe+s(3)*alpha(j,3)*alpha(j,4)*N';  
      Ke=Ke+s(3)*alpha(j,3)*N'*N; 
     end     
    end


  
 K(ind(i,:),ind(i,:))=K(ind(i,:),ind(i,:))+Ke;  % elemento laidumo matrica pridedama prie konstrukcijos laidumo matricos
 F(ind(i,:))=F(ind(i,:))+Fe; % elemento mazgu galiu vektorius pridedamas prie konstrukcijos galiu vektoriaus
   
end

% Konstrukcijos lygciu sistemos spendimas

% laidumo matrica ir galiu vektorius pagal sablona isskaidomi blokais;
% prie lygties desines puses pridedamas galiu vektorius, salygotas duotos temperaturos
T(find(~IS))=K(find(~IS),find(~IS))\(F(find(~IS))-K(find(~IS),find(IS))*T(find(IS)));
T


figure(2);hold on;axis equal; title('Temperature');
konstrukcijos_vaizdavimas(cor,ind,lmbd,b,q,alpha,T,IS,1);

figure(3);hold on;axis equal; title('Temperature');
konstrukcijos_vaizdavimas(cor,ind,lmbd,b,q,alpha,T,IS,2);
return
end


function [A,s,a,b,c,N]=matrica_N(cor,pnt)
% cor - mazgu koordinates
% pnt - tasko, kuriame skaiciuojame formos funkciju matrica, koordinates

A=norm(cross([cor(2,:)-cor(1,:), 0], [cor(3,:)-cor(1,:), 0]))/2; % trikampio plotas
s(1)=norm(cor(2,:)-cor(1,:)); %s12   % krastiniu ilgiai
s(2)=norm(cor(3,:)-cor(2,:)); %s23
s(3)=norm(cor(3,:)-cor(1,:)); %s31

a(1)=cor(2,1)*cor(3,2)-cor(3,1)*cor(2,2);   % formos funkciju koeficientai (pagal analitines formules)
a(2)=cor(3,1)*cor(1,2)-cor(1,1)*cor(3,2);
a(3)=cor(1,1)*cor(2,2)-cor(2,1)*cor(1,2);
b(1)=cor(2,2)-cor(3,2);
b(2)=cor(3,2)-cor(1,2);
b(3)=cor(1,2)-cor(2,2);
c(1)=cor(3,1)-cor(2,1);
c(2)=cor(1,1)-cor(3,1);
c(3)=cor(2,1)-cor(1,1);
a=a/(2*A);b=b/(2*A);c=c/(2*A);

% formos funkciju matricos reiksme, apskaiciuota taske pnt
N=[a(1)+b(1)*pnt(1)+c(1)*pnt(2),a(2)+b(2)*pnt(1)+c(2)*pnt(2),a(3)+b(3)*pnt(1)+c(3)*pnt(2)];

return
end

%----------------------------------------------------------------

function konstrukcijos_vaizdavimas(cor,ind,lmbd,b,q,alpha,T,IS,irez);
% grafiskai pavaizduoja konstrukcija
% cor,ind - geometriniai ir elementu duomenys
% lmbd - laidumo fizikines konstantos
% b,q,alpha - duotos galios ir konvekcijos salygos
% T - mazgu temperaturos
% IS - duotu mazgu temperaturu sablonas (Dirichle krastines salygos)
% irez - kas vaizduojama:
%           0 - iseities duomenys; 
%           1 - temperaturu spalvinis vaizdas; 
%           2 - srauto tankiu vektoriai elementuose 


nel=size(ind,1);nmz=size(cor,1);    % elementu ir masgu skaiciai
nq=size(q,1);nalpha=size(alpha,1);  % duotu pavirsiniu poveikiu (srauto tankiu ir konvekcijos krastiniu) skaiciai

xmax=max(cor(:,1));xmin=min(cor(:,1));  % paveikslo matmenu nustatymas
ymax=max(cor(:,2));ymin=min(cor(:,2));
dx=(xmax-xmin)/10;dy=(ymax-ymin)/10;
axis ([xmin-dx xmax+dx ymin-dy ymax+dy]);

for i=1:nel   % elementu ciklas
    
    % braizomas eilinis konstrukcijos elementas:
    plot(cor([ind(i,:) ind(i,1)],1),cor([ind(i,:) ind(i,1)],2),'k-');
    
    if  irez == 0  % vaizduojami tik iseities duomenys  
        
        % elemento spalva parenkama pagal elemento silumos laidumo koeficienta:
        clr=[0.2 0.9 0.7];fill(cor(ind(i,:),1),cor(ind(i,:),2),clr*i/nel);
        % elemento centre tekstu uzrasomas laidumo koeficientas ir turio saltinio reiksme
        text(mean(cor(ind(i,:),1)),mean(cor(ind(i,:),2)),sprintf('lmbd_%d=%g W/(m*K)\nb_%d=%g W/m^3',i,lmbd(i),i,b(i)));
    
    elseif irez == 1  % elementai uzliejami spalva pagal apskaiciuotas temperaturas
        fill(cor(ind(i,:),1),cor(ind(i,:),2),T(ind(i,:)));colorbar;
        
    elseif irez == 2  % elementu centruose vaizduojami tuose elementuose sklindancio srauto tankio vektoriai
        [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,:),1)),mean(cor(ind(i,:),2)) ]);
        B=[b;c];
        D=[lmbd(i) 0;0 lmbd(i)];
        flux=-D*B*T(ind(i,:));   % apskaiciuojamos srauto tankio reiksmes
        cntr=[mean(cor(ind(i,:),1)),mean(cor(ind(i,:),2)) ]; % elemento centro koordinates
        scale=2e-4;  % parenkamas mastelis srauto tankio vektoriams vaizduoti:
        quiver(cntr(1)-flux(1)*scale/2,cntr(2)-flux(2)*scale/2,flux(1)*scale,flux(2)*scale,'MaxHeadSize',1,'Color','m'); 
        text(cntr(1),cntr(2),sprintf('flux%d=%g',i,norm(flux))); % tekstu uzrasomas srauto tankio vektoriaus modulis
    end
    

    
end

if irez == 0
        for k=1:nq % krastiniu viduryje uzrasomos duotos galio tankio reiksmes
            text(mean(cor(q(k,1:2),1)),mean(cor(q(k,1:2),2)),sprintf('q_{%d%d}=%g W/m^2',q(k,1:2),q(k,3)),'Color','r');
        end
        for k=1:nalpha % krastiniu viduryje uzrasomi duoti konvekcijos koeficientai ir aplinkos temperaturos:
            text(mean(cor(alpha(k,1:2),1)),mean(cor(alpha(k,1:2),2)),...
            sprintf('alpha_{%d%d}=%g W/(m^2*K)\nT_{inf}=%g K',alpha(k,1:2),alpha(k,3:4)),'Color','b');
        end   
end

for i=1:nmz   % ties mazgais zalia spalva uzrasomos apskaiciuotos temperaturu reiksmes
    if ~IS(i)
        if ~irez,    % jeigu vaizduojami iseities duomenys, mezinomos temperaturos zymimos simboliais
            text(cor(i,1),cor(i,2),sprintf('T%d',i),'Color','g');
        else,text(cor(i,1),cor(i,2),sprintf('T%d=%g K',i,T(i)),'Color','g');
        end
    else      % ties duotu temperaturu mazgais magenta spalva uzrasomos duotos reiksmes
        text(cor(i,1),cor(i,2),sprintf('T%d=%g K',i,T(i)),'Color','m');
    end
end

return
end