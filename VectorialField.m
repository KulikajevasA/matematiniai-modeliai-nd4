
function vektorinis_laukas_2D

% apskaiciuoja 2D tamprumo uzdavini, kai naudojami tiesiniai trikampiai elementai

clc, close all

cor=[0 0; 2 0; 1 1; 0 3 ]
ind=[1 2 3; 1 3 4]
edges=Edges(ind)               % surasomos elementu briaunos, vidines pazymimos neigiamu elemento numeriu
edgenum=[1 2;2 3;3 1];         % briaunu tvarka elemente
nel=size(ind,1)
nmz=size(cor,1)

mat=[ 2.1e11  0.3  7800;
      2.1e11  0.3  7800]
for i=1:nel, bf(i,1:3)=[i 0 -mat(i,3)*9.81], end
nf=size(bf,1)

p=[2 3 10000;
   3 4 10000]
np=size(p,1)

for i=1:np
    n=cor(p(i,2),:)-cor(p(i,1),:);
    n=n/norm(n);
    n=[n(2),-n(1)];
    t(i,1:2)=p(i,1:2);
    t(i,3:4)=-n*p(i,3);
end
nt=np
t

forces=[]; forces=[4,-10000*0 , 0];
nf=size(forces,1)


IS=[1 1  0 1  0 0  0 0];
U=zeros(2*nmz,1);U(find(IS))=0;

figure(1);hold on;axis equal; title('Geometry of Structure');
konstrukcijos_vaizdavimas(cor,ind,mat,bf,t,forces,U,IS,0);

K=zeros(2*nmz,2*nmz);F=zeros(2*nmz,1);
for i=1:nel

 [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,:),1)),mean(cor(ind(i,:),2)) ]);
 
 Ke=zeros(6,6);Fe=zeros(6,1);
 E=mat(i,1); nu=mat(i,2);  rho=mat(i,3);
 D=E/(1-nu^2)*[1 nu 0; nu 1 0;  0  0 (1-nu)/2];
  B=[b(1) 0    b(2)   0  b(3)   0;
     0   c(1)   0   c(2)  0    c(3);
     c(1) b(1) c(2) b(2) c(3)  b(3)];
 ib=find(~(bf(:,1)-i)); % kuri bf eilute atitinka sio elemento turinei apkrovai 
 Ke=Ke+A*B'*D*B;
 if ~isempty(ib), Fe=Fe+A*N'*bf(i,2:3)';end

 ind1=zeros(1,6);ind1(1:2:6)=ind(i,1:3)*2-1;ind1(2:2:6)=ind(i,1:3)*2;
 K(ind1,ind1)=K(ind1,ind1)+Ke;
 F(ind1,1)=F(ind1,1)+Fe;
    
end

    for i=1:nt    % ciklas pavirsiniu apkrovu sukurtoms jegoms:
        testarray=bsxfun(@minus,sort(edges(:,1:2),2),sort(t(i,1:2))); % randama, kurio elemento ir kuria siena apkrauna i-oji pavirsine apkrova
        num=find(~sum(abs(testarray),2));
        if ~isempty(num),
        el_nr=edges(num,3);briaunos_nr=edges(num,4);
        [A,s,a,b,c,N]=matrica_N(cor(ind(el_nr,:),:),[mean(cor(ind(el_nr,edgenum(briaunos_nr,:)),1)),mean(cor(ind(el_nr,edgenum(briaunos_nr,:)),2))]);
        ind1=zeros(1,6);ind1(1:2:6)=ind(el_nr,1:3)*2-1;ind1(2:2:6)=ind(el_nr,1:3)*2;
        F(ind1,1)=F(ind1,1)+s(briaunos_nr)*N'*t(i,3:4)';
        end   
        F
    end 
    
% sutelktosios jegos
for i=1:nf,ind1=[forces(i,1)*2-1:forces(i,1)*2]; F(ind1,1)=F(ind1,1)+forces(i,2:3)'; end    
    
U(find(~IS))=K(find(~IS),find(~IS))\(F(find(~IS))-K(     find(~IS),find(IS)   )*U(find(IS)) );
U'

figure(2);hold on;axis equal; title('Stress in Elements');
konstrukcijos_vaizdavimas(cor,ind,mat,bf,t,forces,U,IS,1);

figure(3);hold on;axis equal; title('Nodal Stesses');
konstrukcijos_vaizdavimas(cor,ind,mat,bf,t,forces,U,IS,2);

return
end


function [A,s,a,b,c,N]=matrica_N(cor,pnt)

A=norm(cross([cor(2,:)-cor(1,:), 0], [cor(3,:)-cor(1,:), 0]))/2;
s(1)=norm(cor(2,:)-cor(1,:)); %s12
s(2)=norm(cor(3,:)-cor(2,:)); %s23
s(3)=norm(cor(3,:)-cor(1,:)); %s31

a(1)=cor(2,1)*cor(3,2)-cor(3,1)*cor(2,2);
a(2)=cor(3,1)*cor(1,2)-cor(1,1)*cor(3,2);
a(3)=cor(1,1)*cor(2,2)-cor(2,1)*cor(1,2);
b(1)=cor(2,2)-cor(3,2);
b(2)=cor(3,2)-cor(1,2);
b(3)=cor(1,2)-cor(2,2);
c(1)=cor(3,1)-cor(2,1);
c(2)=cor(1,1)-cor(3,1);
c(3)=cor(2,1)-cor(1,1);
a=a/(2*A);b=b/(2*A);c=c/(2*A);
N1=a(1)+b(1)*pnt(1)+c(1)*pnt(2);
N2=a(2)+b(2)*pnt(1)+c(2)*pnt(2);
N3=a(3)+b(3)*pnt(1)+c(3)*pnt(2);
N=[N1 0 N2 0 N3 0;
   0  N1 0 N2 0 N3];

return
end

%----------------------------------------------------------------


function konstrukcijos_vaizdavimas(cor,ind,mat,bf,t,forces,U,IS,irez);

nel=size(ind,1);nmz=size(cor,1);nt=size(t,1);nbf=size(bf,1);nf=size(forces,1);
IS1= (reshape(IS',2,[]))';

xmax=max(cor(:,1));xmin=min(cor(:,1));ymax=max(cor(:,2));ymin=min(cor(:,2));
dx=(xmax-xmin)/10;dy=(ymax-ymin)/10; mxy=max([dx,dy]);
axis ([xmin-dx xmax+dx ymin-dy ymax+dy]);

S=zeros(nmz,1);AS=zeros(nmz,1);

     % Node numbers
 for i=1:nmz, text(cor(i,1)-mxy/5,cor(i,2)-mxy/5,sprintf('%d',i),'Color','k'); end
    % Element numbers
 for i=1:nel,c=sum(cor(ind(i,:),:),1)/3; text(c(1)+mxy/8,c(2)+mxy/5,sprintf('e%d',i),'Color','k'); end 

 if ~irez    %----------------------
for i=1:nel,  plot(cor([ind(i,:) ind(i,1)],1),cor([ind(i,:) ind(i,1)],2),'k-'); 
              clr=[0.2 0.9 0.7];fill(cor(ind(i,:),1),cor(ind(i,:),2),clr*i/nel,'FaceAlpha',0.5);
end        
                     % BCs:
     if ~isempty(IS)
        for i=1:nmz
         x=cor(i,1);y=cor(i,2);ds=mxy/3;
         if IS1(i,1),plot([x,x],[y-ds,y+ds],'k-','Linewidth',2);end         
         if IS1(i,2),plot([x-ds,x+ds],[y,y],'k-','Linewidth',2);end
        end
     end
     % Surface loads:
     if ~isempty(t)
        for i=1:nt
         c=sum(cor(t(i,1:2),:),1)/2;xxs(i)=c(1);yys(i)=c(2);qxxs(i)=t(i,3);qyys(i)=t(i,4);
         text(xxs(i),yys(i),sprintf('%g',norm(t(i,3:4))),'Color','r');
        end
        aa=max(sqrt(sum(t(3:end).^2,2))); sc=mxy/aa*2; % quiver mastelis
        quiver(xxs-qxxs*sc,yys-qyys*sc,qxxs*sc,qyys*sc,'Autoscale','off','MaxHeadSize',1.5,'Color','r','LineWidth',2);
     end 

     % Body loads:
     if ~isempty(bf)
        for i=1:nbf;
         c=sum(cor(ind(bf(i,1),:),:),1)/3; xxb(i)=c(1);yyb(i)=c(2);qxxb(i)=bf(i,2);qyyb(i)=bf(i,3);
         text(xxb(i),yyb(i),sprintf('%g',norm(bf(i,2:3))),'Color','b');
        end
        aa=max(sqrt(sum(bf(2:end).^2,2))); sc=mxy/aa*2; % quiver mastelis
        h=quiver(xxb,yyb,qxxb*sc,qyyb*sc,'Autoscale','off','MaxHeadSize',1.5,'Color','b','LineWidth',2);
     end  
     
     % Forces
   
     if ~isempty(forces)
        for i=1:nf
         nm=forces(i,1); xxf(i)=cor(nm,1);yyf(i)=cor(nm,2);qxxf(i)=forces(i,2);qyyf(i)=forces(i,3);
         text(xxf(i),yyf(i),sprintf('%g',norm(forces(i,2:3))),'Color','k');
        end
        aa=max(sqrt(sum(forces(2:end).^2,2))); sc=mxy/aa*2; % quiver mastelis
        quiver(xxf-qxxf*sc,yyf-qyyf*sc,qxxf*sc,qyyf*sc,'Autoscale','off','MaxHeadSize',1.5,'Color','k','LineWidth',2);
     end 
     
     
    end %---------------------------------




for i=1:nel
    plot(cor([ind(i,:) ind(i,1)],1),cor([ind(i,:) ind(i,1)],2),'k-');
    if ~irez
        clr=[0.2 0.9 0.7];fill(cor(ind(i,:),1),cor(ind(i,:),2),clr*i/nel);
    elseif irez >= 1
        ind1=[2*ind(i,1)-1  2*ind(i,1) 2*ind(i,2)-1  2*ind(i,2) 2*ind(i,3)-1  2*ind(i,3)] ;
        Ue=U(ind1);
        scale=100000;
        [A,s,a,b,c,N]=matrica_N(cor(ind(i,:),:),[mean(cor(ind(i,:),1)),mean(cor(ind(i,:),2)) ]);
        E=mat(i,1); nu=mat(i,2);  rho=mat(i,3);
        D=E/(1-nu^2)*[1 nu 0;
               nu 1 0;
               0  0 (1-nu)/2];
  B=[b(1) 0    b(2)   0  b(3)   0;
     0   c(1)   0   c(2)  0    c(3);
     c(1) b(1) c(2) b(2) c(3)  b(3)];
        sg=D*B*Ue;
        seqv=sqrt((sg(1)-sg(2))^2/2 +sg(1)^2/2+sg(2)^2/2 +3*sg(3)^2);
        if irez==1 fill(cor(ind(i,:),1)+Ue([1 3 5])*scale,cor(ind(i,:),2)+Ue([2 4 6])*scale,seqv,'FaceAlpha',1); colorbar; end
        S(ind(i,:))=S(ind(i,:))+seqv*A;
        AS(ind(i,:))=AS(ind(i,:))+A;
    end
end
        if irez == 2
            S=S./AS;
            for i=1:nel
                plot(cor([ind(i,:) ind(i,1)],1),cor([ind(i,:) ind(i,1)],2),'k-');
                ind1=[2*ind(i,1)-1  2*ind(i,1) 2*ind(i,2)-1  2*ind(i,2) 2*ind(i,3)-1  2*ind(i,3)] ;
                Ue=U(ind1);
                fill(cor(ind(i,:),1)+Ue([1 3 5])*scale,cor(ind(i,:),2)+Ue([2 4 6])*scale,S(ind(i,:)),'FaceAlpha',1); colorbar; 
            end
        end
        
    
return
end


function edges=Edges(ind)
% surasomos elementu briaunos, vidines pazymimos neigiamu elemento numeriu:
nel=size(ind,1);ii=0;
for i=1:nel
    ii=ii+1;edges(ii,1:4)=[ind(i,[1 2]),i,1];   % briaunos mazgai, el.nr ir briaunos nr.elemente
    if ii > 1,testarray=bsxfun(@minus,sort(edges(1:ii-1,1:2),2),sort(ind(i,[1 2])));
        num=find(~sum(abs(testarray),2));
        if num, edges([num,ii],3)=-edges([num,ii],3);end
    end
    ii=ii+1;edges(ii,1:4)=[ind(i,[2 3]),i,2];
    testarray=bsxfun(@minus,sort(edges(1:ii-1,1:2),2),sort(ind(i,[2 3])));
        num=find(~sum(abs(testarray),2));
        if num, edges([num,ii],3)=-edges([num,ii],3);end
    ii=ii+1;edges(ii,1:4)=[ind(i,[3 1 ]),i,3];
    testarray=bsxfun(@minus,sort(edges(1:ii-2,1:2),2),sort(ind(i,[3 1])));
        num=find(~sum(abs(testarray),2));
        if num, edges([num,ii],3)=-edges([num,ii],3);end    
end

return
end