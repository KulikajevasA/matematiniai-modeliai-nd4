function pvz_1_FEmodel_SteadyFlow 
clc, clear all
close all,

% Skyscio fizikiniai rodikliai (vanduo 20C):
rho=1000;  % tankis
mu=1e-3 ;  % dinaminis klampos koeficientas 
g=9.81;    % laisvo kritimo pagreitis 

model=0; % modelio varijantas 0 arba 1
switch model
    case 0 
    L=10;      % vamzdzio ilgis
    alpha=pi/6*0;  % vamzdzio kampas su horizontu
    nL=5;      % elementu skaicius
    Dmin=0.2;Dmax=0.2; %Pradzios ir galo diametrai
    DD=[Dmin:(Dmax-Dmin)/nL:Dmax]; % vamzdziu skersmenys
    if isempty(DD), DD=Dmin*ones(nL,1);end
    AA=pi*DD.^2/4; % vamzdzio elementu skerspjuvio plotai  
    Le=L/nL;   % elemento igis
    x=[0:Le:L]*cos(alpha);y=[0:Le:L]*sin(alpha);
    cor=[x',y'];nmz=size(cor,1)
    ind=[1:nmz-1;2:nmz]'
    nel =size(ind,1), nmz =size(cor,1),

    IS=zeros(nmz,1);   % �ablonas duot� slegiu mazgams pa�ym�ti:
    IS([1,end])=1;
    Win=zeros(nmz,1); % duotas iejimo debitu mazguose vektorius
%    Win([1,end])=[100,-100];
    P=zeros(nmz,1); % Sukuriamas slegiu masyvas
    P(find(IS))=[1e4;0];

    case 1    
    L1=10;L2=10;      % vamzdziu ilgiai
    nL1=6;nL2=6;      % vamzdziu elementu skaiciai
    alpha1=pi/6*2;alpha2=-pi/6*1;  % vamzdziu kampai su horizontu
    Dmin1=0.1;Dmax1=0.1;Dmin2=0.1;Dmax2=0.1;Daux=0.05; %Pradzios, galo ir jungiancio vamzdzio diametrai
    DD1=[Dmin1:(Dmax1-Dmin1)/nL1:Dmax1];DD2=[Dmin2:(Dmax2-Dmin2)/nL2:Dmax2]; % vamzdziu skersmenys
    if isempty(DD1), DD1=Dmin1*ones(nL1,1);end
    if isempty(DD2), DD2=Dmin2*ones(nL2,1);end
    DD=[DD1',DD2',Daux];
    AA1=pi*DD1.^2/4;AA2=pi*DD2.^2/4; Aaux=pi*Daux.^2/4; % vamzdzio elementu skerspjuvio plotai  
    AA=[AA1',AA2',Aaux];
    Le1=L1/nL1;Le2=L2/nL2;   % elementu igiai
    x=[[0:Le1:L1]*cos(alpha1),L1*cos(alpha1)+[Le2:Le2:L2]*cos(alpha2)];
    y=[[0:Le1:L1]*sin(alpha1),L1*sin(alpha1)+[Le2:Le2:L2]*sin(alpha2)];
    cor=[x',y'];nmz=size(cor,1)
    iin1=ceil(nL1/2)+1;iin2=nL1+ceil(nL2/2)+1; % jungiancio elemento mazgai 
    ind=[[1:nmz-1;2:nmz]';[iin1,iin2]]
    nel =size(ind,1), nmz =size(cor,1),
    IS=zeros(nmz,1);   % �ablonas duot� slegiu mazgams pa�ym�ti:
    IS([1,end])=1;
    Win=zeros(nmz,1); % duotas iejimo debitu mazguose vektorius
%    Win([1,end])=[100,-100];
    P=zeros(nmz,1); % Sukuriamas slegiu masyvas
    P(find(IS))=[1e5;0];
end

% Konstrukcijos vaizdas:
figure(1); set(gcf,'Color','white'); grid on; hold on; box on;
spalva1=[0 0.8 1];spalva2=[0 0 0];
for iii=1:nel 	% ciklas per elementus pavaizdavimui
    i=ind(iii,1);j=ind(iii,2); Le=norm(cor(j,:)-cor(i,:));
    cc=(cor(j,:)+cor(i,:))/2; cd=cor(j,:)-cor(i,:);
    braizyti_staciakampi(cc(1),cc(2),atan2(cd(2),cd(1)),Le,DD(iii),spalva1);
end
plot(x,y,'o','Markersize',6,'Linewidth',2,'Color',spalva2); axis equal;
%----------------------------------------------------------------

vE=zeros(1,nel); vE1=zeros(1,nel);

 for ijk=1:1000000  % netiesines lygties sprendimo iteraciju ciklas (paprastosios iteracijos)

     K=zeros(nmz);S=zeros(nmz,1);
    for iii=1:nel 	% ciklas per elementus, konstrukcijos matricu surinkimas
        D=DD(iii);A=AA(iii);i=ind(iii,1);j=ind(iii,2);Le=norm(cor(j,:)-cor(i,:));
        Re=abs(rho*vE(iii)*D/mu);
        if Re==0, kkk(iii)=1;
        else
            if Re<2500, kkk(iii)=Re/(64*abs(vE(iii))); 
            else, kkk(iii)=Re^(1/4)/(0.316*abs(vE(iii))); 
            end  % nuostoliu koeficientas
        end
        Ke=kkk(iii)*2*D*A/Le*[1,-1;-1,1];
        Se=kkk(iii)*2*D*A/Le*rho*g*(cor(j,2)-cor(i,2))*[1;-1];
        K([i,j],[i,j])=K([i,j],[i,j])+Ke;
        S([i,j])=S([i,j])+Se;
    end 
%    Lygciu sistemos sprendimas:
    P(find(~IS))=K(find(~IS),find(~IS))\(S(find(~IS))-K(find(~IS),find(IS))*P(find(IS)));

    for iii=1:nel 	% ciklas per elementus greiciu nustatymui
        D=DD(iii);A=AA(iii); i=ind(iii,1);j=ind(iii,2); Le=norm(cor(j,:)-cor(i,:));
        WE(iii)=kkk(iii)*2*D*A/Le*(P(i)-P(j) -rho*g*(cor(j,2)-cor(i,2)));
        vE1(iii)=WE(iii)/(A*rho);
    end
    eps=norm(vE1-vE)/norm(vE); if eps < 1e-6, itsk=ijk;break; end
    vE=vE+0.6*(vE1-vE);
 end
 
for iii=1:nel 	% ciklas per elementus debitu ir greiciu reiksmiu uzrasymui 
    i=ind(iii,1);j=ind(iii,2); cc=(cor(j,:)+cor(i,:))/2;
    text(cc(1),cc(2),sprintf('w=%g\nv=%g',WE(iii),vE(iii)),'Backgroundcolor','white')
    Lvec=cor(j,:)-cor(i,:); Lvec=Lvec/norm(Lvec);
    xq(iii)=cc(1);yq(iii)=cc(2);uq(iii)=vE(iii)*Lvec(1);vq(iii)=vE(iii)*Lvec(2);
   
end
quiver(xq,yq,uq,vq,0.1,'Color','red');
 
for iii=1:nmz 	% ciklas per mazgus slegio reiksmiu uzrasymui
    text(cor(iii,1)+2*DD(max(1,iii-1)),cor(iii,2),sprintf('p%-3d=%g',iii,P(iii)),'Color',[0 0.6 0],'Backgroundcolor','white');
end

if ~isempty(itsk), 
    P',WE,vE
    title(sprintf('1D tekme vamzdyne apskaiciuota per %d iteraciju',itsk));
else
    '*** sprendinys nesukonvergavo'
end
return
end

function hndl=braizyti_staciakampi(xc,yc,phi,a,b,spalva)
    coord=[-a/2  a/2  a/2 -a/2;
           -b/2 -b/2  b/2  b/2;
             1    1    1    1  ];   % staciakampis etalonineje padetyje   
    T=[cos(phi) -sin(phi) xc;
       sin(phi)  cos(phi) yc;
          0         0      1 ];      % transformavimo matrica
    coord=T*coord;
    ih=1;  hndl(ih)=fill(coord(1,:),coord(2,:),spalva);
    return
end


