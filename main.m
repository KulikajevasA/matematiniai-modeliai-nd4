clear all; clc; close all; set(0,'DefaultFigureColormap',feval('hot'));

% 2 ----- 1
% | \ a / |
% | b 5 d |
% | / c \ |
% 3 ----- 4


% 2 --4-- 1
%  \ / \ /
%   5 - 6
%    \ /
%     3
vertices = [7 7; 2 7; 2 1; 7 1; 4.5 4];
indices = [1 2 5; 2 3 5; 3 4 5; 4 1 5];
% indices = [1 2 5];

a1 = 20; a2 = 22;
lmbd1 = 54; lmbd2 = 24;
b1 = 0; b2 = 150000;
h = 0.05;
T_env = 300;
Q_output = -25000;
T_source = 350;

alpha = [a1 a2 a2 0];
lambda = [lmbd1 lmbd2 lmbd2 lmbd1];
bodySource = [b2 b2 b1 b2];

subdivisions = 4;

IS = [1 0 0 0 0];
GLOBAL = [0 0 1 0 0];

% -----------    END OF INPUT     -----------

[vertices, indices, alpha, lambda, bodySource, IS, GLOBAL] = triangulate( ...
    vertices, indices, alpha, lambda, bodySource, IS, GLOBAL, subdivisions);

elements = size(indices, 1);
nodes = size(vertices, 1);

T = zeros(nodes, 1); T(find(IS)) = T_source;
K = zeros(nodes, nodes); F = zeros(nodes, 1);  
%F(find(GLOBAL)) = Q_output;

% ----------- END OF PERPARATIONS -----------

for i = 1 : elements
    [A, N, B] = shapeFunctionMatrix(vertices(indices(i, :), :),  ...
                                        [mean(vertices(indices(i, :), 1)), ...
                                         mean(vertices(indices(i, :), 2))] ...
                                    );

    V = A * h;
                   
    D  = [lambda(i) 0; 0 lambda(i)];
    
    
   
    Ke = V * B' * D * B + A * alpha(i) * eye(3, 3) / 3; % N' * N;                   % formule
    
    Pe = V * bodySource(i) * N'; % ones(3, 1) / 3;
    Se0 = A * alpha(i) * T_env * N';
    
    Fe = Pe + Se0;
    
    
    K(indices(i, :), indices(i,:)) = K(indices(i, :), indices(i, :)) + Ke;  
    F(indices(i, :)) = F(indices(i, :)) + Fe;
   
end

T(find(~IS))=  K(find(~IS), find(~IS)) \ (F(find(~IS)) - K(find(~IS), find(IS)) * T(find(IS)));




draw(vertices, indices, T);

% ----------- END OF CALCULATIONS -----------

function [A, N, B] = shapeFunctionMatrix(vertices, point)
    x = point(1); y = point(2);

    ax = vertices(1, 1); ay = vertices(1, 2);
    bx = vertices(2, 1); by = vertices(2, 2);
    cx = vertices(3, 1); cy = vertices(3, 2);
    
    A = abs(((by-cy)*ax+(cy-ay)*bx+(ay-by)*cx)/2);
    
    inv2A = 1 / (2*A);
    
    a1 = inv2A * (bx*cy-cx*by); a2 = inv2A * (cx*ay-ax*cy); a3 = inv2A * (ax*by-bx*ay);
    b1 = inv2A * (by-cy); b2 = inv2A * (cy - ay); b3 = inv2A * (ay-by);
    c1 = inv2A * (cx-bx); c2 = inv2A * (ax - cx); c3 = inv2A * (bx-ax);
    
    N1 = a1 + b1 * x + c1 * y;
    N2 = a2 + b2 * x + c2 * y;
    N3 = a3 + b3 * x + c3 * y;
    
    N = [N1 N2 N3];
    B = [b1 b2 b3; c1 c2 c3];
end

function [vertices, indices, alpha, lambda, bodySource, IS, GLOBAL] = triangulate( ...
            vertices, indices, alpha, lambda, bodySource, IS, GLOBAL, count)
    map = containers.Map('KeyType', 'char', 'ValueType', 'any');
    lastVert = 1;
    % eps = 1e-10;
    

    splitIndices = [1 4 6; 4 2 5; 5 6 4; 5 3 6];

    for h = 1 : count    
        lastFace = size(indices, 1);

        
    IS_source = find(IS);
    GLOBAL_source = find(GLOBAL);
        
        for i = 1 : lastFace
            tmpVerts = vertices(indices(i,:), :);

            vd = (tmpVerts(1, :) + tmpVerts(2, :)) / 2;
            ve = (tmpVerts(2, :) + tmpVerts(3, :)) / 2;
            vf = (tmpVerts(1, :) + tmpVerts(3, :)) / 2;

            verts = vertcat(tmpVerts, [vd; ve; vf]);

            newIndices = zeros(1, 12);
            counter = 1;

            for j = 1 : 4
                face = splitIndices(j, :);
                for k = 1 : 3
                    v = verts(face(k), :);
                    str = num2str(v, 15);

                    if map.isKey(str)
                        tmp = map(str);
                        index = tmp(1);
                    else
                        index = lastVert;
                        map(str) = [lastVert v(1) v(2)];
                        lastVert = lastVert + 1;
                    end

                    newIndices(counter) = index;
                    counter = counter + 1;
                end
            end

            indices(i, :) = newIndices(1:3);

            for j = 1 : 3
                offset = j * 3;
                additional = newIndices(offset+1:offset+3);
                indices(end+1, :) = additional;
                alpha(end+1) = alpha(i);
                lambda(end+1) = lambda(i);
                bodySource(end+1) = bodySource(i);
            end
        end

        vvals = map.values();
        vcount = size(vvals);
        remapped = zeros(vcount(2), 2);

        for i = 1 : vcount(2)
            tmp = vvals(i);
            tmp = [tmp{1, :}];
            
            vrtx = [tmp(2) tmp(3)];
            
            remapped(tmp(1), :) = vrtx;
            
            if vrtx == vertices(GLOBAL_source, :)
                GLOBAL = zeros(vcount(2), 1);
                GLOBAL(tmp(1)) = 1;
            end
            
            if vrtx == vertices(IS_source, :)
                IS = zeros(vcount(2), 1);
                IS(tmp(1)) = 1;
            end
        end

        vertices = remapped;
        
        %IS = zeros(size(vertices, 1), 1);
        %IS(IS_source) = 1;
    end
end

function draw(vertices, indices, T)
    figure(1); hold on; axis equal;

    totalElements = size(indices, 1);
    %totalVertices = size(vertices, 1);

    for i = 1 : totalElements
        plot( ...
        vertices([indices(i,:) indices(i,1)], 1), ...
        vertices([indices(i,:) indices(i,1)], 2), ...
        'k-');
    
        fill(vertices(indices(i,:),1),vertices(indices(i,:),2),T(indices(i,:)));colorbar;
    
        %T(indices(i, :))
        %alpha(indices(i,:))
        %fill(vertices(indices(i,:),1),vertices(indices(i,:),2),bodySource(i));colorbar;
    end
end